FROM node:16

ENV LANG=en_US.utf8

RUN npm install -g @hyperspace/cli

COPY docker-entrypoint.sh /usr/local/bin
USER node:node
ENTRYPOINT [ "docker-entrypoint.sh" ]
STOPSIGNAL SIGINT

CMD [ "hyp", "info", "-l", "--live" ]